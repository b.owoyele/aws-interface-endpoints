# AWS Interface Endpoints

##### **About**   
VPC Endpoints allow services within a VPC to access AWS services and other supported services over the **private AWS network**. 

##### **Benefits**   
- Increased security secondary to not having to use the public internet to access services  
- Cost-saving:
    * Data transfer is less expensive over a private network 
    * No costs associated with the use of a NAT Gateway
    * No costs associated with maintaining firewalls
- Avoid internet gateway bottleneck
- Lower latency
- High availability 
- Horizontal scaling 


##### **Two Endpoint Types**
* Interface Endpoint
* Gateway Endpoint 


##### **Interface v. Gateway Endpoints**
* Interface 
    * Endpoint access control via resource policy AND security group
    * Better for on-premise/hybrid solutions. (The ENI is a part of the VPC with a private IP address that is within the associated subnet and therefore can support VPNs and Direct Connect)
    * Supports many services 
* Gateway 
    * Endpoint access control via resource policy only
    * Better for cloud-only solutions. (The endpoint is created outside of the VPC and the traffic is routed via route table and therefore unable to support VPNs and Direct Connect)
    * Support DynamoDB and S3 only
    * Endpoint and services must be in the same region 

##### **Service VPC Endpoints (VPC Private Link)**
- Most secure way to expose a service VPC to 100s of VPCs **privately**
- Better solution than VPC peering because peering is limited by overlapping CIDR ranges of the connecting VPCs
- Requirements:
    * Interface endpoint on the customer side
    * Service endpoint and network load balancer on the service provider side

# **Architecture**
 ![Endpoint](InterfaceEndpoint.png))


# **Creating an RDS Database via Interface Endpoint Hands-On**
**Environment Set-up**
1. In the AWS console navigate to the VPC dashboard and create a VPC with the following:
    * 1 public subnet 
    * 1 private subnet
    * 1 internet gateway attached to the VPC
    * 1 route table associated with the public subnet and routed to the internet gateway
    * 1 route table associated with the private subnet

2. Launch an instance in the public and private subnet each.     
In the AWS console navigate to the EC2 dashboard > Instances > Launch instances  
For the public instance:
    * Step 1: Choose Amazon Linux 2 AMI 
    * Step 2: Choose t2.micro as the instance type
    * Step 3: Under the Network dropdown select the VPC created in the environment set-up
    * Step 4: Under the Subnet dropdown select the public subnet created in the environment set-up 
    * Step 5: Keep the default storage configuration
    * Step 6: Add a tag. Key= Name, Value= bastionhost 
    * Step 7: Create a new security group and configure the source as my IP address only on port 22. Name the security group bastionhostSG
    * Step 8: Launch the instance 
    * Step 9: Download and save the instance keypair 

    For the private instance:  
    * Step 1: Choose Amazon Linux 2 AMI
    * Step 2: Choose t2.micro as the instance type
    * Step 3: Under the Network dropdown select the VPC created in the environment set-up
    * Step 4: Under the Subnet dropdown select the private subnet created in the environment set-up 
    * Step 5: Keep the default storage configuration
    * Step 6: Add a tag. Key= Name, Value= privateinstance 
    * Step 7: Create a new security group and configure the source as the bastion host security group only on port 22. Name the security group privateSG
    * Step 8: Launch the instance 

3. Ensure DNS hostname and resolution are enabled 
In the AWS console navigate to the VPC dashboard > Select Your VPCs 
    * Step 1: Select the created VPC
    * Step 2: Select the Actions dropdown 
    * Step 3: Select Edit DNS hostnames > Choose Enable > Save 
    * Step 4: Select Edit DNS resolution > Choose Enable > Save 

4. Create an IAM role and attach it to the private instance.
In the AWS console navigate to the IAM dashboard > Select Roles > Create Role
    * Step 1: Choose Trusted Entity Type as AWS Service > Select EC2 as the use case > Next 
    * Step 2: Search RDS under Permission policies > Choose AmazonRDSFullAccess > Next
        *  Chose full access for the purpose of this demo but when creating an IAM role in a real situation be sure to exercise the **rule of least privilege** for best security practice
    * Step 3: Name the Role > Create
    * Step 4: Return to Instance dashboard > Select the private instance > Actions > Security > Modify IAM role > Choose created role > Save 

5. Create an Interface Endpoint 
In the AWS console navigate to the VPC dashboard > Select Endpoints
    * Step 1: Select Create endpoint 
    * Step 2: Name the endpoint > select AWS Service under Service category 
    * Step 3: Search RDS under services > Select RDS Interface (com.amazonaws.us-east-1.rds)
    * Step 4: Select the created VPC 
    * Step 5: Select the availability zone of your private instance > select your private instance in the dropdown
    * Step 6: Select the default security group
        * The default security group that's created will only allow traffic from the VPC
        * Chose the default for the purpose of this demo but when creating a security group in a real situation be sure to exercise the **rule of least privilege** for best security practice
    * Step 7: Select the default resource policy 
        * Chose the default for the purpose of this demo but when creating a resource policy in a real situation be sure to exercise the **rule of least privilege** for best security practice 
    * Step 8: Select Create

6.  Connect to the private instance via bastion host (public instance)
    - [For details on how to connect to a private instance via bastion host](https://gitlab.com/b.owoyele/bastion-host-within-a-private-vpc)

7. Run `aws configure` in the private instance CLI to access the AWS via CLI
    * Enter the access key ID
    * Enter the secret access key 
    * Enter the default region (optional)
    * Enter the default output (optional)

8. Create the RDS database using the following command
```
aws rds create-db-instance --db-name firstrds --db-instance-identifier my-demo-mysql-instance --db-instance-class db.t3.micro --engine mysql --master-username demo --master-user-password demo54321 --allocated-storage 5
```

9. Should recieve an output of details regarding the RDS database configuration in the CLI. To double check go to the RDS dashboard to see if the databaase was created. 

10. Delete the RDS database using the following command 
```
aws rds delete-db-instance --db-instance-identifier my-demo-mysql-instance --skip-final-snapshot
```



